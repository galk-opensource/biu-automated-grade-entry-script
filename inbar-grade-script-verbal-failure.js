

var PASS = 60;  // This is the default Pass/Fail threshold, change if needed.

//This is the grades dictionary
var grades={};


grades[666]=60; //example, replace with your grades


// -----------------------------------
// DO NOT MODIFY CODE BELOW THIS LINE
// -----------------------------------

function graderow(row,checkdigit)
{
    var id = row.children[0].innerText;
    if (!checkdigit)
        id = removelastdigit(id);
    if (id in grades) {
        if (grades[id]>=PASS) {
            row.children[2].children[0].value = grades[id];
        } else {
            row.children[3].children[0].value = 1;
            row.children[4].children[0].value = grades[id];
        }
    }
    
}

function removelastdigit(id)
{
    return id.slice(0, -1);
}

function fillgrades(checkdigit=true)
{
    var table =document.getElementById("ContentPlaceHolder1_gvLessonGrades");
    //number of students+1 (first row is title)
    var rowsnum = table.children[0].children.length;
    //loop through students
    for (i=1; i<rowsnum;i++)
        graderow(table.children[0].children[i],checkdigit);
}




//fill the grades. If the id numbers do not have a check digit replace with: fillgrades(false);
fillgrades();
