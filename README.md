# README #

This repository holds a script and instructions for automating entry of grades
in the the Bar Ilan University INBAR (inbar.biu.ac.il) system.  For some reason, the design of the system
allows manual entry of grades only.  This is a ridiculous waste of time for
lecturers and TAs who have the grades in front of them in some worksheet (e.g., a CSV file)
and need to copy the grades one by one.

## Credits

[Dr. Erez Sheiner](https://math.biu.ac.il/en/node/677), a lecturer at the Bar Ilan's math department, has created a script
which **almost** automates the process, and makes grade entry a fun exercise in working
the browser console tools.  Erez's original code is given [in this file](inbar-grade-script-numeric-failure.js). 

A second version ("verbal failure") enters "FAIL" status for grades below 60 is also given, and puts the original
number grades in the comments field. This script is given [in this file](inbar-grade-script-verbal-failure.js) 
Credit for the second version is due to [Dr. Yael Amsterdamer](https://www.cs.biu.ac.il/~amstery/), of the computer science department at BIU.

## Instructions

The original operating instructions are in Hebrew, and given as a PDF generated from Erez's
original email.  Despite the disclaimer that the script may not work with more than 130 students,
I've successfully run it with groups of more than 170 students.

In firefox, the developer tools are opened with ctrl-shift-i, rather than ctrl-shift-j in chrome (as stated in the instructions).
Also, I've often received "undefined" at the end of the run, which can be mistaken as an indication of an error -- but the grades are inserted.  So ignore

An example empty XLS (Excel) file is provided.  The student ID and grade go into the empty two columns (A, B).  The C column entries should be copied into the script per the instructions.

The instructions are identical to the "verbal failure" script, with the exception that the
pass/fail threshold can be modified. The default is 60. The threshold can be modified by changing
the variable at the top of the script.





