
//This is the grades dictionary
var grades={};  // Do not modify

// Copy the C column entries here, in bulk

grades["666"]=69;   //example, replace with your grades



// -----------------------------------
// DO NOT MODIFY CODE BELOW THIS LINE
// -----------------------------------
function graderow(row,checkdigit)
{
    var id = row.children[0].innerText;
    if (!checkdigit)
        id = removelastdigit(id);
    if (id in grades)
        row.children[2].children[0].value = grades[id];
}

function removelastdigit(id)
{
    return id.slice(0, -1);
}

function fillgrades(checkdigit=true)
{
    var table =document.getElementById("ContentPlaceHolder1_gvLessonGrades");
    //number of students+1 (first row is title)
    var rowsnum = table.children[0].children.length;
    //loop through students
    for (i=1; i<rowsnum;i++)
        graderow(table.children[0].children[i],checkdigit);
}



//fill the grades. If the id numbers does not have a check digit replace with: fillgrades(false);
fillgrades();
